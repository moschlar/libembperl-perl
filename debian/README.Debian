Debian-specific notes for Embperl
=================================

Thread safety (and possible options)
-------------

NB: Embperl is not threadsafe and will not work with any of apache2's
    threaded models in mod_perl mode.  In Debian terms, if you want to
    use libembperl-perl with apache2 and mod_perl2, you must enable the
    prefork mpm.

If you are running a threaded flavor of Apache, or if you are using
any other webserver, you should run Embperl through the FastCGI-based
application server.

This package has been compiled with support for mod_perl and libxslt.
The Embperl CGI scripts are installed into /usr/lib/cgi-bin.

Crypto support
--------------

See crypto/README in the source package for instructions on building
with source encryption support.  Since this feature relies on storing
the encryption key in the executable, there is no way to ship this
precompiled.

Deprecation notice
------------------

Note that, as of late 2007, upstream development of Embperl has
halted; Embperl is regarded as stable and well-behaving, and it is
still supported in Debian. However, we urge you to think twice before
using it for new systems development.

 -- Gunnar Wolf <gwolf@debian.org>, Thu, 15 Oct 2009 11:39:18 -0500

Forms examples and libjs-prototype
----------------------------------

The example code for Embperl::Forms, which is installed in
/usr/share/doc/libembperl-perl/examples/forms/, includes an embedded
copy of the Prototype JavaScript Framework library. In Debian, this
library is shipped as part of the libjs-prototype package, and its
copy in libembperl-perl was replaced by a symlink.

Thus if you want to run the forms example, please install the
libjs-prototype package suggested by libembperl-perl.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Tue, 02 Oct 2012 22:12:33 +0200
